<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>yrdy</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">

        <!-- Styles -->
        <style>
            html, body {
                background-color: #fff;
                color: #636b6f;
                font-family: 'Nunito', sans-serif;
                font-weight: 200;
                height: 100vh;
                margin: 0;
            }
            body {
                display: flex;
                justify-content: center;
                align-items: center;
                flex-direction: column;
            }
            .link {
                margin-bottom: 20px;
            }
            .form {
                display: flex;
                flex-direction: column;
            }
            .form *:not(:first-child) {
                margin-top: 10px;
            }
            label {
                display: flex;
                flex-direction: column;
            }
        </style>
    </head>
    <body>
    @if (isset($link) && $link)
    <div class="link">
        <a href="{{$link}}">{{$link}}</a>
    </div>
    @endif
    <form action="./" class="form">
        <label for="">
            Ссылка
            <input type="text" name="origin">
        </label>
        <label for="">
            Короткая ссылка (не обязательно)
            <input type="text" name="encoded">
        </label>
        <label for="">
            Срок сгорания ссылки (не обязательно)
            <input type="datetime-local" name="expire">
        </label>
        <input type="submit" value="Уменьшить">
    </form>
    </body>
</html>
