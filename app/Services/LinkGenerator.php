<?php

namespace App\Services;

use App\Link;
use phpDocumentor\Reflection\Types\Boolean;

class LinkGenerator
{
    const CHARS = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    public function encode(): string {
        $result = '';

        $charactersLength = strlen(self::CHARS);
        for ($i = 0; $i < 7; $i++) {
            $result .= self::CHARS[rand(0, $charactersLength - 1)];
        }

        if ($this->existUrl($result)) {
            return $this->encode();
        }

        return $result;
    }

    private function existUrl(string $str): bool
    {
        return Link::whereEncoded($str)->exists();
    }

}
