<?php

namespace App\Http\Controllers;

use App;
use App\Link;
use App\Services\LinkGenerator;
use Illuminate\Http\Request;

class IndexController extends Controller
{
    const ALLOWED_PARAMS = ['origin', 'expire', 'encoded'];
    private $generator;

    public function __construct(LinkGenerator $generator)
    {
        $this->generator = $generator;
    }


    public function index(Request $request)
    {
        $data = array_intersect_key($request->all(), array_flip(self::ALLOWED_PARAMS));
        $link_url = false;

        if ($data) {

            if (empty($data['encoded'])) {
                $data['encoded'] = $this->generator->encode();
            }

            if (empty($data['expire'])) {
                $data['expire'] = date('Y-m-d H:i:s', strtotime('+1 hours'));
            }

            $link = new Link($data);
            $link->save();
            $link_url = config('app.url') . '/' . $link->encoded;

        }



        return view('welcome')->with('link', $link_url);
    }

    public function slug($slug)
    {
        if ($link = Link::query()->where('origin', $slug)->where('expire', '>', date('Y-m-d H:i:s'))->first()) {
            return view('welcome')->with('link', config('app.url') . '/' . $link->encoded);
        }

        if ($link = Link::query()->where('encoded', $slug)->where('expire', '>', date('Y-m-d H:i:s'))->first()) {
            return redirect($link->origin);
        }

        abort(404);
    }
}
